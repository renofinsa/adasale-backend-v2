'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('productAds', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      productId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "products",
          key: "id"
        }
      },
      meta: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      started: {
        type: Sequelize.DATE,
        allowNull: false
      },
      expired: {
        type: Sequelize.DATE,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('productAds');
  }
};