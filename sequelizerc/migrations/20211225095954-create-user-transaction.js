'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('userTransactions', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING(25)
      },
      userId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "users",
          key: "id"
        }
      },
      isPaid: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      via: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('userTransactions');
  }
};