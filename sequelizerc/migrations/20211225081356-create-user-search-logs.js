'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('userSearchLogs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "users",
          key: "id"
        }
      },
      keyword: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      category: {
        type: Sequelize.STRING(25),
        allowNull: false
      },
      location: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('userSearchLogs');
  }
};