'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('useTransactionItems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userTransactionId: {
        type: Sequelize.STRING(25),
        references: {
          model: "userTransactions",
          key: "id"
        }
      },
      title: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      price: {
        type: Sequelize.INTEGER(20),
        allowNull: false
      },
      productId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "products",
          key: "id"
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('useTransactionItems');
  }
};