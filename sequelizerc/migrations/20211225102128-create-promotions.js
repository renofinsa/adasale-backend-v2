'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('promotions', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING(25)
      },
      categoryId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "categories",
          key: "id"
        }
      },
      duration: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      price: {
        type: Sequelize.INTEGER(25),
        allowNull: false
      },
      typeId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "promotionMeta",
          key: "id"
        }
      },
      ispublish: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('promotions');
  }
};