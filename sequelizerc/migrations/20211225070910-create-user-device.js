'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('userDevices', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "users",
          key: "id"
        }
      },
      ipAaddress: {
        type: Sequelize.STRING(25),
        allowNull: false
      },
      media: {
        type: Sequelize.STRING(100),
        allowNull: false
      },
      token: {
        type: Sequelize.INTEGER(100),
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('userDevices');
  }
};