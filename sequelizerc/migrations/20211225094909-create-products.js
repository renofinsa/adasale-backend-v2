'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('products', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING(25)
      },
      userId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "users",
          key: "id"
        }
      },
      title: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      description: {
        type: Sequelize.STRING(100),
        allowNull: false
      },
      slug: {
        type: Sequelize.STRING(100),
        allowNull: false
      },
      categoryId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "categories",
          key: "id"
        }
      },
      statusId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "products",
          key: "id"
        }
      },
      userAddressId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "userAddresses",
          key: "id"
        }
      },
      isDeleted: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('products');
  }
};