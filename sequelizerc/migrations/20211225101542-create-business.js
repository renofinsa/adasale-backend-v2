'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('businesses', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING(25)
      },
      name: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      description: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      price: {
        type: Sequelize.INTEGER(25),
        allowNull: false
      },
      typeId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "promotionMeta",
          key: "id"
        }
      },
      isPublish: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('businesses');
  }
};