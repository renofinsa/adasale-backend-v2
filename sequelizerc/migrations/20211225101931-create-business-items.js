'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('businessItems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      businessId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "businesses",
          key: "id"
        }
      },
      typeId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "promotionMeta",
          key: "id"
        }
      },
      duration: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('businessItems');
  }
};