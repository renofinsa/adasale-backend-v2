'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('userProperties', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.STRING(25),
        allowNull: false,
        references: {
          model: "users",
          key: "id"
        }
      },
      meta: {
        type: Sequelize.STRING(25),
        allowNull: false
      },
      content: {
        type: Sequelize.STRING(100),
        allowNull: false
      },
      isPublish: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('userProperties');
  }
};