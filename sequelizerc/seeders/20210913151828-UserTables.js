'use strict';

const bcrypt = require('bcrypt')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return Promise.all([
      // queryInterface.bulkInsert('users', [
      //   {
      //     name: 'Reno Finsa',
      //     phoneNumber: "08111899619",
      //     email: 'reno.finsa@gmail.com',
      //     password: bcrypt.hashSync('admin', 12)
      //   }
      // ], {})
     ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return Promise.all([
      queryInterface.bulkInsert('users', null, {}),
    ])
  }
};
