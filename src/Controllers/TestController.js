// REQUIRED
const { response } = require('express');
const User = require('./../Models/User'); // panggil model nya disini

exports.get = async (req, res) => {
  try {
    // QUERY
    // dan deklarasikan model nya disini, serta kirimpan req jika diperlukan
    const data = await User.get({ userId: req.query.id });

    // RESPONSE
    return res.success({ message: 'Users', data: data });
  } catch (error) {
    // RESPONSE ERROR
    return res.error({ code: 503, error });
  }
};

exports.store = async (req, res) => {
  try {
    //query
    const data = await User.post(req);

    //response
    return res.success({ message : 'User has been created', data: data});
  } catch (error) {
    return res.error({ error : 503, error});
  }
}

exports.update = async (req, res) => {
  try {
    //query
    const data = await User.update(req);
    //response
    return res.success({ message: 'User has been update', data: data});
  } catch (error) {
    return response.error({code: 503, error });
  }
}


exports.destroy = async (req, res) => {
  try {
    // QUERY
    const data = await User.destroy(req);

    // IF NOT FOUND
    if (!data) {
      return res.error({ code: 404, message: 'user not found!', error: 'Not found!' });
    }

    // RESPONSE
    return res.success({ message: 'User has been deleted', data: data });
  } catch (error) {
    // RESPONSE ERROR
    return res.error({ code: 503, error });
  }
};
