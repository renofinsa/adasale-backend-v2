// REQUIRED
const User = require('./../Models/User');
const userStoreValidation = require('./../Validations/userStoreValidation')
const userUpdateValidation = require('./../Validations/userUpdateValidation')

exports.get = async (req, res) => {
  try {
    // QUERY
    const data = await User.get({ userId: req.query.id });

    // RESPONSE
    return res.success({ message: 'Users', data: data });
  } catch (error) {
    // RESPONSE ERROR
    return res.error({ code: 503, error });
  }
};

exports.show = async (req, res) => {
  try {
    // QUERY
    const data = await User.get({ userId: req.params.id });
    
    // IF 
    if (!data) {
      return res.error({ code: 404, message: 'user not found!', error: 'Not found!' });
    }
    
    // RESPONSE
    return res.success({ message: 'User detail', data: data });
  } catch (error) {
    // RESPONSE ERROR
    return res.error({ code: 503, error });
  }
};

exports.store = async (req, res) => {
  try {
    // CHECK EMAIL
    var user = await User.getByEmail({ email: req.body.email })
  
    // CHECK VALIDATION
    const { errors, isValid } = userStoreValidation(req.body, { email: !user ? null : user.email })
    if (!isValid) {
      return res.error({ error: errors });
    }
  
    // QUERY
    const data = await User.store(req);
    // RESPONSE
    return res.success({ message: 'User has been created', data: data });

  } catch (error) {
    // RESPONSE ERROR
    return res.error({ code: 503, error });
  }
};

exports.update = async (req, res) => {
  try {
    // CHECK EMAIL
    var user = await User.getByEmail({email: req.body.email, userId: req.params.id})
  
    // CHECK VALIDATION
    const { errors, isValid } = userUpdateValidation(req.body, { email: !user ? null : user.email })
    if (!isValid) {
      return res.error({ error: errors });
    }
    
    // QUERY
    const data = await User.update(req);

    // RESPONSE
    return res.success({ message: 'User has been updated', data: data });
  } catch (error) {
    // RESPONSE ERROR
    return res.error({ code: 503, error });
  }
};

exports.destroy = async (req, res) => {
  try {
    // QUERY
    const data = await User.destroy(req);

    // IF NOT FOUND
    if (!data) {
      return res.error({ code: 404, message: 'user not found!', error: 'Not found!' });
    }

    // RESPONSE
    return res.success({ message: 'User has been deleted', data: data });
  } catch (error) {
    // RESPONSE ERROR
    return res.error({ code: 503, error });
  }
};
