// SEQUELIZE
const Models = require('../Models');
const { users } = Models;
const sequelize = Models.Sequelize;
const { Op } = sequelize;

const bcrypt = require('bcrypt')

exports.get = async ({ userId = null }) => {
  try {
    // kondisi jika single user
    if (!userId) {
      var data = await users.findAll({
        // attributes adalah nama field yang ingin di munculkan
        attributes: ['id', 'name', 'email', 'phoneNumber', 'createdAt', 'updatedAt'],
        // order adalah urutan berdasarkan field tertentu
        order: [['id', 'DESC']]
      });
      
      // return data ke controller
      return data;
    }

    // kondisi jika multiple user
    var data = await users.findOne({
      where: {id: userId},
      attributes: ['id', 'name', 'email', 'phoneNumber', 'createdAt', 'updatedAt']
    });
    
    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

exports.post = async (req) => {
  try {
    const {
      id,
      name,
      email,
      password,
      photoProfile,
      phoneNumber,
      username,
      referenceId
    } = req.body;

    const data = await users.create({
      id: id,
      name: name,
      email: email,
      password: bcrypt.hashSync(password,12),
      phoneNumber: phoneNumber,
      photoProfile: photoProfile,
      username: username,
      referenceId: referenceId
    });
    return data;

  } catch (error) {
    console.log(error);
    throw error;
  }
}


exports.update = async (req) => {
  try {
    const {
      name,
      email,
      password,
      photoProfile,
      phoneNumber,
      username
    } = req.body;

    const data = await users.update({
      name: name,
      email: email,
      password: bcrypt.hashSync(password,12),
      phoneNumber: phoneNumber,
      photoProfile: photoProfile,
      username: username    },
    { where: { id: req.params.id } }
  );
    return data;

  } catch (error) {
    console.log(error);
    throw error;
  }
}



exports.destroy = async (req) => {
  try {
    const data = await users.findOne({ where: {id: req.params.id} });
    
    if (data) {
      data.destroy();
      return true;
    }

    return false;
  } catch (error) {
    console.log(error);
    throw error;
  }
}

