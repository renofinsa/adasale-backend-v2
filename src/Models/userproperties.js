'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userProperties extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  userProperties.init({
    userId: DataTypes.STRING,
    meta: DataTypes.STRING,
    content: DataTypes.STRING,
    isPublish: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'userProperties',
  });
  return userProperties;
};