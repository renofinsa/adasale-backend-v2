'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userRef extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  userRef.init({
    primaryId: DataTypes.STRING,
    referenceId: DataTypes.STRING,
    value: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'userRef',
  });
  return userRef;
};