'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userDevice extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  userDevice.init({
    userId: DataTypes.STRING,
    ipAaddress: DataTypes.STRING,
    media: DataTypes.STRING,
    token: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'userDevice',
  });
  return userDevice;
};