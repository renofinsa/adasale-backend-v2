'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userVerify extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  userVerify.init({
    userId: DataTypes.STRING,
    meta: DataTypes.STRING,
    code: DataTypes.INTEGER,
    verifyDate: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'userVerify',
  });
  return userVerify;
};