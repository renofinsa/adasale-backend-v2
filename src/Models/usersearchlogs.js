'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userSearchLogs extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  userSearchLogs.init({
    userId: DataTypes.STRING,
    keyword: DataTypes.STRING,
    category: DataTypes.STRING,
    location: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'userSearchLogs',
  });
  return userSearchLogs;
};