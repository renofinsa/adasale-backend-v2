'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class promotions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // User.belongsTo(models.categories, {
      //   as: 'categories',
      //   foreignKey: 'categoryId',
      //   sourceKey: 'id'
      // });
    }
  };
  promotions.init({
    id: {
      type: DataTypes.STRING,
      primaryKey:true
    },
    categoryId: DataTypes.STRING,
    duration: DataTypes.STRING,
    price: DataTypes.INTEGER,
    typeId: DataTypes.STRING,
    ispublish: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'promotions',
  });
  return promotions;
};