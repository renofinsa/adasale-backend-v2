'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class promotionMeta extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      promotionMeta.hasMany(models.business, {
        as: 'business',
        foreignKey: 'typeId',
        sourceKey: 'id'
      });
      promotionMeta.hasMany(models.businessItems, {
        as: 'businessItems',
        foreignKey: 'typeId',
        sourceKey: 'id'
      });
      promotionMeta.hasMany(models.promotions, {
        as: 'promotions',
        foreignKey: 'typeId',
        sourceKey: 'id'
      });
    }
  };
  promotionMeta.init({
    id: {
      type: DataTypes.STRING,
      primaryKey:true
    },
    meta: DataTypes.STRING,
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'promotionMeta',
  });
  return promotionMeta;
};