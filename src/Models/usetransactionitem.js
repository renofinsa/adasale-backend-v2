'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class useTransactionItem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      useTransactionItem.belongsTo(models.userTransaction, {
        as: 'userTransaction',
        foreignKey: 'userTransactionId',
        sourceKey: 'id'
      });
    }
  };
  useTransactionItem.init({
    userTransactionId: DataTypes.STRING,
    title: DataTypes.STRING,
    price: DataTypes.INTEGER,
    productId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'useTransactionItem',
  });
  return useTransactionItem;
};