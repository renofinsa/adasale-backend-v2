'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userPromotion extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // userPromotion.belongsTo(models.categories, {
      //   as: 'categories',
      //   foreignKey: 'categoryId',
      //   sourceKey: 'id'
      // });
    }
  };
  userPromotion.init({
    userId: DataTypes.STRING,
    name: DataTypes.STRING,
    duration: DataTypes.STRING,
    expired: DataTypes.DATE,
    type: DataTypes.STRING,
    categoryId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'userPromotion',
  });
  return userPromotion;
};