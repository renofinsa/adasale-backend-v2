'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class products extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // products.hasMany(models.productImage, {
      //   as: 'productImage',
      //   foreignKey: 'productId',
      //   sourceKey: 'id'
      // });
      // products.hasMany(models.productLogs, {
      //   as: 'productLogs',
      //   foreignKey: 'productId',
      //   sourceKey: 'id'
      // });
      // products.hasMany(models.productCart, {
      //   as: 'productCart',
      //   foreignKey: 'productId',
      //   sourceKey: 'id'
      // });
      // products.hasMany(models.productProperties, {
      //   as: 'productProperties',
      //   foreignKey: 'productId',
      //   sourceKey: 'id'
      // });
      // products.hasMany(models.productAttributes, {
      //   as: 'productAttributes',
      //   foreignKey: 'productId',
      //   sourceKey: 'id'
      // });
      // products.hasMany(models.productAds, {
      //   as: 'productAds',
      //   foreignKey: 'productId',
      //   sourceKey: 'id'
      // });
      // products.belongsTo(models.product, {
      //   as: 'product',
      //   foreignKey: 'productId',
      //   sourceKey: 'id'
      // });
      // products.belongsTo(models.categories, {
      //   as: 'categories',
      //   foreignKey: 'categoryId',
      //   sourceKey: 'id'
      // });
      // products.belongsTo(models.userAddress, {
      //   as: 'userAddress',
      //   foreignKey: 'userAddressId',
      //   sourceKey: 'id'
      // });
    }
  };
  products.init({
    id: {
      type: DataTypes.STRING,
      primaryKey:true
    },
    userId: DataTypes.STRING,
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    slug: DataTypes.STRING,
    categoryId: DataTypes.STRING,
    statusId: DataTypes.STRING,
    userAddressId: DataTypes.STRING,
    isDeleted: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'products',
  });
  return products;
};