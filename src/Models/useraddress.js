'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userAddress extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  userAddress.init({
    userId: DataTypes.STRING,
    title: DataTypes.STRING,
    province: DataTypes.STRING,
    city: DataTypes.STRING,
    village: DataTypes.STRING,
    address: DataTypes.STRING,
    postalCode: DataTypes.INTEGER,
    isSelected: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'userAddress',
  });
  return userAddress;
};