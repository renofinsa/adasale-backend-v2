'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // User.hasMany(models.userLogs, {
      //   as: 'logs',
      //   foreignKey: 'userId',
      //   sourceKey: 'id'
      // });
      // User.hasMany(models.userDevice, {
      //   as: 'device',
      //   foreignKey: 'userId',
      //   sourceKey: 'id'
      // });
      // User.hasMany(models.userAddress, {
      //   as: 'address',
      //   foreignKey: 'userId',
      //   sourceKey: 'id'
      // });
      // User.hasMany(models.userDevice, {
      //   as: 'device',
      //   foreignKey: 'userId',
      //   sourceKey: 'id'
      // });
      // User.hasMany(models.userVerify, {
      //   as: 'verify',
      //   foreignKey: 'userId',
      //   sourceKey: 'id'
      // });
      // User.hasMany(models.userProperties, {
      //   as: 'properties',
      //   foreignKey: 'userId',
      //   sourceKey: 'id'
      // });
      // User.hasMany(models.userSearchLogs, {
      //   as: 'searchLogs',
      //   foreignKey: 'userId',
      //   sourceKey: 'id'
      // });
      // User.hasMany(models.userTransaction, {
      //   as: 'transaction',
      //   foreignKey: 'userId',
      //   sourceKey: 'id'
      // });
      // User.hasMany(models.userSearchLogs, {
      //   as: 'searchLogs',
      //   foreignKey: 'userId',
      //   sourceKey: 'id'
      // });
    }
  };
  User.init({
    id: {
      type: DataTypes.STRING,
      primaryKey:true
    },
    photoProfile: DataTypes.STRING,
    name: DataTypes.STRING,
    username: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    referenceId: DataTypes.STRING,
    isActive: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'users'
    // timestamps: false
  });
  return User;
};