'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class productAds extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  productAds.init({
    id: {
      type: DataTypes.STRING,
      primaryKey:true
    },
    productId: DataTypes.STRING,
    meta: DataTypes.STRING,
    started: DataTypes.DATE,
    expired: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'productAds',
  });
  return productAds;
};