'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class productMeta extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  productMeta.init({
    id: {
      type: DataTypes.STRING,
      primaryKey:true
    },
    meta: DataTypes.STRING,
    content: DataTypes.STRING,
    isPublish: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'productMeta',
  });
  return productMeta;
};