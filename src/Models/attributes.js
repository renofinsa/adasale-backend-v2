'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class attributes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  attributes.init({
    id: {
      type: DataTypes.STRING,
      primaryKey:true
    },
    meta: DataTypes.STRING,
    parentId: DataTypes.INTEGER,
    content: DataTypes.STRING,
    isDeleted: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'attributes',
  });
  return attributes;
};