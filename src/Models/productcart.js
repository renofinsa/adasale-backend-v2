'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductCart extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ProductCart.belongsTo(models.products, {
        as: 'products',
        foreignKey: 'productId',
        sourceKey: 'id'
      });
    }
  };
  ProductCart.init({
    id: {
      type: DataTypes.STRING,
      primaryKey:true
    },
    userId: DataTypes.STRING,
    productId: DataTypes.STRING,
    qty: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'ProductCart',
  });
  return ProductCart;
};