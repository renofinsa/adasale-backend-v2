'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class businessItems extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      businessItems.belongsTo(models.business, {
        as: 'business',
        foreignKey: 'businessId',
        sourceKey: 'id'
      });
    }
  };
  businessItems.init({
    businessId: DataTypes.STRING,
    typeId: DataTypes.STRING,
    duration: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'businessItems',
  });
  return businessItems;
};