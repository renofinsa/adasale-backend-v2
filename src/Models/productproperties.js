'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class productProperties extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  productProperties.init({
    productId: DataTypes.STRING,
    meta: DataTypes.STRING,
    content: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'productProperties',
  });
  return productProperties;
};