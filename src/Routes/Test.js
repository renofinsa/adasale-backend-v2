module.exports = () => {
  const TestController = require('./../Controllers/TestController'); // panggil controller nya
  const app = require('express').Router();

  // lalu deklarasikan disini
  app.get('/', TestController.get);
  app.post('/store', TestController.store);
  app.patch('/:id', TestController.update);
  app.delete('/:id', TestController.destroy);


  return app;
};
