module.exports = () => {
  const UserController = require('./../Controllers/UserController');
  const app = require('express').Router();

  app.get('/', UserController.get);
  app.get('/:id', UserController.show)
  app.post('/', UserController.store);
  app.patch('/:id', UserController.update);
  app.delete('/:id', UserController.destroy);

  return app;
};
