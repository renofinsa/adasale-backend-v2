const app = require('express').Router();
const routes = (name) => require(`./${name}`)

/** Write ur routes here */

app.get('/', (req, res) => {
  return res.success({ message: 'Lets Code' })
})

app.use('/user', routes('User')());
app.use('/test', routes('Test')()); // deklarasikan routes nya disini, sesuaikan dengan nama file nya

module.exports = app