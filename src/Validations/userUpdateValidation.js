const Validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function LoginValidation(data, value = null) {
  let errors = {}

  data.name = !isEmpty(data.name) ? data.name : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.phoneNumber = !isEmpty(data.phoneNumber) ? data.phoneNumber : '';

  if (Validator.isEmpty(data.name)) {
    errors.name = 'Name is required';
  };

  if (Validator.isEmpty(data.email)) {
    errors.email = 'Email is required';
  };

  if (!Validator.isEmail(data.email)) {
    errors.email = 'Email incorrect format';
  };

  if (Validator.equals(data.email, value.email)) {
    errors.email = 'Email is not available'
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = 'Password tidak boleh kosong';
  };

  if (!Validator.equals(data.password, data.verifyPassword)) {
    errors.email = 'Password should be match';
  };

  if (Validator.isEmpty(data.phoneNumber)) {
    errors.phoneNumber = 'Phone number is required';
  };

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
