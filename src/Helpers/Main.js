// exam 1
exports.reverse = function (character) {
  var value = '';
  for (let i = character.length - 1; i >= 0; i--) {
    value += character[i];
  }

  return value;
}

// exam 2
exports.fibonacci = function (n) {
  var fib = [0, 1];
  for(var i=fib.length; i<n; i++) {
    fib[i] = fib[i-2] + fib[i-1];
  }

  var value = fib.join().toString().replaceAll(","," ");
  return value;
}  

// exam 3
exports.combination = function (n, r) {
  if (n == r) {
    return 1;
  } else {
    r=(r < n-r) ? n-r : r;
    return range(r+1, n)/range(1,n-r);
  }
}

// range result for combination
const range = (a, b) => {
  var value = a,i = a;
 
  while (i++< b) {
    value*=i;
  }

  return value;
}