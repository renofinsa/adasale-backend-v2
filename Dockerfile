FROM node:12

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
VOLUME ["/public/"]

# ADD . /path/inside/docker/container
COPY package*.json ./

RUN npm cache clean --force && npm install -g npm@latest --force && npm cache clean --force && rm -rf node_modules package-lock.json && npm install && npm install bcrypt@5.0.0 && npm install -g pm2
# RUN npm install && npm install -g pm2

COPY . .

EXPOSE 8080

CMD [ "pm2-runtime", "--env", "production", "ecosystem.config.json" ]
