// config
require('dotenv').config();

// packages
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const db = require('./src/Models');

// files
const Response = require('./src/Helpers/Response');

// init app config
const app = express();
app.use(cookieParser('rahasia'));
app.use(bodyParser.json({ strict: true, limit: '1mb' }));
app.set('x-powered-by', false);
app.set('trust proxy', true);

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Method', 'GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, Cookie, Accept, Origin, X-Requested-With');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

// allow public directory
app.use('/public', express.static(path.join(__dirname, '/public')));

// routes
app.use(Response);
app.use('/api/v1', require('./src/Routes'));

app.get('/', (req, res) => {
  return res.success({ data: null, message: 'https://developer.mozilla.org/en-US/docs/Web/HTTP/Status', status: 202 });
});

// running
app.listen(process.env.PORT, () => {
  db.sequelize.sync();
  console.log('Server started at :', process.env.PORT);
});
